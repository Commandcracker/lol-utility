# LOL utility

LOL utility is a python program that access the lcu API \
to do some cool stuff to your league \
\
It has a fancy qt GUI shown here: \
\
![bStats](form.png)

## Disclaimer

**This Application might get your riot account banned**

## Installation

install [Python](https://www.python.org/downloads/release)

```bash
# use python3 or python depends on the installation

# install pip
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

# download the requirements
python -m pip install --upgrade -r requirements.txt
```
