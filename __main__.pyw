#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from shutil import rmtree
from json import load, dump
from os.path import join, dirname, exists
from os import mkdir, remove, getpid
from signal import signal, SIGINT, SIG_DFL
from sys import platform
from threading import Thread

# pip modules
import psutil
from PySide6.QtCore import QFile, Qt
from PySide6.QtGui import QIcon
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import QApplication, QMainWindow
from qt_material import apply_stylesheet


# local modules
from riotgames_api import LeagueOfLegendsClientAPI, LeagueOfLegendsExternalAPI, get_process_by_name

# Variables

# Paths
file_path = dirname(__file__)

data_folder_path = join(file_path, "data")

images_folder_path = join(data_folder_path, "images")
version_file_path = join(data_folder_path, "version.txt")
settings_file_path = join(data_folder_path, "settings.json")

ui_file_path = join(file_path, "form.ui")
icon_file_path = join(file_path, "icon.ico")

# API`s
lolcapi = LeagueOfLegendsClientAPI()
loleapi = LeagueOfLegendsExternalAPI(
    lolcapi.get("/riotclient/region-locale").get("locale"))

lobbys = {}
champions = {}

positions = {
    "TOP": 0,
    "JUNGLE": 1,
    "MID": 2,
    "BOTTOM": 3,
    "SUPPORT": 4
}

# Settings
settings = {}

# Threading
exit_flag = False


def change_settings(inp, key):
    settings[key] = inp


class lol(QMainWindow):
    def __init__(self):
        super(lol, self).__init__()

        ui_file = QFile(ui_file_path)
        ui_file.open(QFile.ReadOnly)
        self.window = QUiLoader().load(ui_file, self)
        ui_file.close()

        # auto_accept
        self.window.auto_accept.setChecked(bool(settings.get("auto_accept")))
        self.window.auto_accept.toggled.connect(
            lambda inp: change_settings(inp, "auto_accept"))

        # auto_search
        self.window.auto_search.setChecked(bool(settings.get("auto_search")))
        self.window.auto_search.toggled.connect(
            lambda inp: change_settings(inp, "auto_search"))

        # auto_lobby
        self.window.auto_lobby.setChecked(bool(settings.get("auto_lobby")))
        self.window.auto_lobby.toggled.connect(
            lambda inp: change_settings(inp, "auto_lobby"))

        # lobbys
        for lobby in lobbys:
            self.window.lobby.addItem(lobby)

        for key, value in lobbys.items():
            if value == settings.get("lobby"):
                self.window.lobby.setCurrentText(key)

        self.window.lobby.currentTextChanged.connect(
            lambda text: change_settings(
                lobbys.get(text),
                "lobby"
            )
        )

        # pick & ban
        for champion in champions:
            icon = QIcon(join(images_folder_path, champion + ".png"))
            self.window.pick.addItem(icon, champion)
            self.window.ban.addItem(icon, champion)

        self.window.pick.setCurrentText(
            get_champion_name_by_id(settings.get("pick")))
        self.window.ban.setCurrentText(
            get_champion_name_by_id(settings.get("ban")))

        self.window.pick.currentTextChanged.connect(
            lambda text: change_settings(
                int(champions.get(text).get("key")),
                "pick"
            )
        )
        self.window.ban.currentTextChanged.connect(
            lambda text: change_settings(
                int(champions.get(text).get("key")),
                "ban"
            )
        )

        # auto_ban
        self.window.auto_ban.setChecked(bool(settings.get("auto_ban")))
        self.window.auto_ban.toggled.connect(
            lambda b: change_settings(
                b,
                "auto_ban"
            )
        )

        # auto_pick
        self.window.auto_pick.setChecked(bool(settings.get("auto_pick")))
        self.window.auto_pick.toggled.connect(
            lambda b: change_settings(
                b,
                "auto_pick"
            )
        )

        # chat
        for position in positions:
            self.window.chat.addItem(position)

        for key, value in positions.items():
            if value == settings.get("chat"):
                self.window.chat.setCurrentText(key)

        self.window.chat.currentTextChanged.connect(
            lambda text: change_settings(
                positions.get(text),
                "chat"
            )
        )

        # auto_chat
        self.window.auto_chat.setChecked(bool(settings.get("auto_chat")))
        self.window.auto_chat.toggled.connect(
            lambda inp: change_settings(inp, "auto_chat"))

        # boost
        self.window.boost.setChecked(bool(settings.get("boost")))
        self.window.boost.toggled.connect(
            lambda inp: change_settings(inp, "boost"))


class main_loop:
    def __init__(self):
        last_gameflow_phase = None
        summonerId = lolcapi.get(
            "/lol-summoner/v1/current-summoner").get("summonerId")

        while True:
            if exit_flag:
                break
            gameflow_phase = lolcapi.get("/lol-gameflow/v1/gameflow-phase")
            if gameflow_phase == "None" and settings.get("auto_lobby"):
                lolcapi.post("/lol-lobby/v2/lobby",
                             {"queueId": settings.get("lobby")})
            elif gameflow_phase == "Lobby" and settings.get("auto_search"):
                lolcapi.post("/lol-lobby/v2/lobby/matchmaking/search")
            elif gameflow_phase == "ReadyCheck" and settings.get("auto_accept"):
                lolcapi.post("/lol-matchmaking/v1/ready-check/accept")
            elif gameflow_phase == "ChampSelect":
                session = lolcapi.get("/lol-champ-select/v1/session")

                if last_gameflow_phase != gameflow_phase and settings.get("auto_chat"):
                    for team in session.get("myTeam"):
                        if team.get("summonerId") == summonerId:
                            if team.get("assignedPosition") == "":
                                chat(get_position_by_id(settings.get("chat")))
                                break

                if session.get("actions") is not None and (settings.get("auto_ban") or settings.get("auto_pick")):
                    localPlayerCellId = session.get("localPlayerCellId")
                    for actions in session.get("actions"):
                        for action in actions:
                            if action.get("actorCellId") == localPlayerCellId and action.get("completed") == False:
                                if action.get("type") == "ban" and settings.get("auto_ban"):
                                    lolcapi.patch("/lol-champ-select/v1/session/actions/" + str(action.get("id")),
                                                  {'championId': settings.get("ban")})
                                    lolcapi.post(
                                        "/lol-champ-select/v1/session/actions/" + str(action.get("id")) + "/complete")
                                elif action.get("type") == "pick" and settings.get("auto_pick"):
                                    lolcapi.patch("/lol-champ-select/v1/session/actions/" + str(action.get("id")),
                                                  {'championId': settings.get("pick")})
                                    lolcapi.post(
                                        "/lol-champ-select/v1/session/actions/" + str(action.get("id")) + "/complete")

            elif gameflow_phase == "InProgress" and last_gameflow_phase != gameflow_phase and settings.get("boost"):
                get_process_by_name("League of Legends").nice(
                    psutil.HIGH_PRIORITY_CLASS
                )

            last_gameflow_phase = gameflow_phase


def chat(message):
    while True:
        conversations = lolcapi.get("/lol-chat/v1/conversations")
        for conversation in conversations:
            if conversation.get("type") == "championSelect":
                lolcapi.post("/lol-chat/v1/conversations/" +
                             conversation.get("pid") + "/messages", {"body": message})
                return


def get_champion_name_by_id(id):
    for key, value in champions.items():
        if value.get("key") == str(id):
            return key


def get_position_by_id(id):
    for key, value in positions.items():
        if value == int(id):
            return key


class app:
    def __init__(self):
        self.app = QApplication([])

        apply_stylesheet(self.app, theme='dark_amber.xml')

        self.app.setApplicationName(
            "LOL utility - " + str.upper(lolcapi.get("/system/v1/builds").get("patchline")))
        self.app.setWindowIcon(QIcon(icon_file_path))

        widget = lol()
        widget.setWindowFlags(widget.windowFlags() |
                              Qt.MSWindowsFixedSizeDialogHint)
        widget.show()
        try:
            self.app.exec()
            # kill whole process to terminate threads
            dump(settings, open(settings_file_path, 'w'))
            psutil.Process(getpid()).kill()
        except KeyboardInterrupt:
            # kill whole process to terminate threads
            dump(settings, open(settings_file_path, 'w'))
            psutil.Process(getpid()).kill()


if __name__ == "__main__":
    # make qt and Threads CTRL+C Interruptable
    # https://stackoverflow.com/questions/5160577/ctrl-c-doesnt-work-with-pyqt#5160720
    if platform != "win32":
        signal(SIGINT, SIG_DFL)

    # Data folder #
    if not exists(data_folder_path):
        mkdir(data_folder_path)

    # Settings #
    if exists(settings_file_path):
        settings = load(open(settings_file_path, 'r'))

    # Lobby's
    for queue in lolcapi.get("/lol-game-queues/v1/queues"):
        if queue.get("queueAvailability") == "Available":
            if queue.get("detailedDescription"):
                lobbys[queue.get("detailedDescription")] = queue.get("id")
            else:
                lobbys[queue.get("name")] = queue.get("id")

    # champions
    # lolcapi.get("/lol-champions/v1/owned-champions-minimal")
    champions = loleapi.get_champions().get("data")

    # Path
    if exists(version_file_path):
        file = open(version_file_path, 'r')
        version = file.read()
        file.close()
        if version != loleapi.version:
            remove(version_file_path)
            rmtree(images_folder_path)

    if not exists(images_folder_path):
        mkdir(images_folder_path)
        for champ in champions:
            file = open(join(images_folder_path, champ + ".png"), "wb")
            print("Downloading " + file.name)
            file.write(loleapi.get_Champion_Square_Assets(champ))
            file.close()

    if not exists(version_file_path):
        file = open(version_file_path, 'w')
        file.write(loleapi.version)
        file.close()

    # Threading
    t1 = Thread(target=app)
    t2 = Thread(target=main_loop)
    t1.start()
    t2.start()
    t1.join()
    exit_flag = True
